import { CURRENCY } from 'constants/currency'
import { RequestData, RatesType } from './index.d'

const rounding = (value: number) => Number(value.toFixed(6))

const formatBases = (data: RequestData): RatesType => {
	const basesArray: any = {
		[data.base]: data.rates,
	}

	for (const mainCurrency in CURRENCY) {
		if (mainCurrency === data.base) continue

		basesArray[mainCurrency] = {
			[data.base]: rounding(1 / basesArray[data.base][mainCurrency]),
		}

		for (const dependedCurrency in CURRENCY) {
			if (mainCurrency === data.base || dependedCurrency === mainCurrency) continue

			const baseCurrency = basesArray[data.base][dependedCurrency]
			const kk = basesArray[mainCurrency][data.base]

			basesArray[mainCurrency][dependedCurrency] = rounding(baseCurrency * kk)
		}
	}

	return basesArray
}

export const currencyFormatter = (data: RequestData): RatesType => formatBases(data)
