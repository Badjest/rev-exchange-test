import { CURRENCY } from 'constants/currency'

export type CURRENCY_KEYS = keyof typeof CURRENCY

export interface RequestData {
	disclaimer: string
	license: string
	timestamp: number
	base: CURRENCY_KEYS
	rates: {
		[key in CURRENCY_KEYS]: number
	}
}

export type RatesType = {
	[key in CURRENCY_KEYS]: {
		[key in CURRENCY_KEYS]: number
	}
}
