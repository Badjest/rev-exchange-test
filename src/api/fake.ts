const random = (min: number, max: number) => {
	let rand = min - 0.5 + Math.random() * (max - min + 1)

	return Math.round(rand)
}

const fakeRequest = async <T>(result: T): Promise<T> => {
	await new Promise((resolve: Function) => setTimeout(resolve, random(400, 1100)))

	if (process.env.NODE_ENV !== 'production') {
		console.log(result)
	}

	return new Promise((resolve) => resolve(result))
}

const doExchange = (data: any) => fakeRequest(data)

export default {
	doExchange,
}
