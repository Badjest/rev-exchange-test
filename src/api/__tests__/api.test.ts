import { getRates, BASE_URL, API_KEY } from 'api/currencyRequest'
import FAKE_API from 'api/fake'
import { FetchMock } from 'jest-fetch-mock'
import { RATES_AFTER_FORMAT, RATES_RESPONSE_DATA } from 'test/mockData'

const fetchMock = fetch as FetchMock

describe('API testing', () => {
	beforeEach(() => {
		fetchMock.resetMocks()
	})

	it('getRates', async () => {
		fetchMock.mockResponseOnce(JSON.stringify(RATES_RESPONSE_DATA))

		const result = await getRates()
		const url: string = fetchMock.mock.calls[0][0]

		expect(url.includes(BASE_URL)).toBeTruthy()
		expect(url.includes(API_KEY)).toBeTruthy()
		expect(fetchMock.mock.calls[0][1].method).toEqual('GET')
		expect(result).toMatchObject(RATES_AFTER_FORMAT)
	})

	it('fake API', async () => {
		fetchMock.mockResponseOnce('1')

		const result = await FAKE_API.doExchange(RATES_RESPONSE_DATA)

		expect(result).toMatchObject(RATES_RESPONSE_DATA)
		expect(fetchMock.mock.calls.length).toEqual(0)
	})
})
