import { CURRENCY } from 'constants/currency'
import { requestFactory } from 'utils/request'
import { currencyFormatter } from './formatter'

export const BASE_URL = 'https://openexchangerates.org/api/'

// I know that not safe to write API KEY in code,
// in real app, I would write it in .env file,
// but it's test app and API KEY is trial
export const API_KEY = 'ea1522e5e1e04a0f8bc85db01ba4d10c'
const SYMBOLS = Object.keys(CURRENCY).join(',')

const request = requestFactory(BASE_URL)

export const getRates = () =>
	request('latest.json', {
		data: {
			app_id: API_KEY,
			symbols: SYMBOLS,
		},
		responseFormatter: currencyFormatter,
	})
