import React from 'react'
import { Provider } from 'react-redux'
import ReactDOM from 'react-dom'
import App from 'containers/App'
import GlobalStyles from 'GlobalStyles'
import store from 'store/index'
import SnackbarProvider from 'containers/SnackbarProvider'

ReactDOM.render(
	<>
		<GlobalStyles />
		<Provider store={store}>
			<SnackbarProvider />
			<App />
		</Provider>
	</>,
	document.getElementById('root'),
)
