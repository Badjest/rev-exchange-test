import appendQueryParams from './appendQueryParams'

interface RequestData {
	[key: string]: any
}

interface RequestOptions<T, S> {
	method?: 'GET' | 'POST' | 'PUT' | 'PATH' | 'OPTIONS' | 'PATCH'
	headers?: HeadersInit
	data?: RequestData
	body?: BodyInit | null
	dataType?: 'json' | 'blob'
	responseFormatter?(data: S): T
}

export const HEADERS = { 'Content-Type': 'application/json' }

const requestFactory = (BASE_URL = '') => async <T, S = any>(
	uri: string,
	opts: RequestOptions<T, S> = {},
): Promise<T> => {
	const { method = 'GET', body, data, headers, responseFormatter, dataType = 'json' } = opts

	const preformattedUrl = `${BASE_URL}${uri}`
	const isGet = method.toUpperCase() === 'GET'
	const url = isGet && data ? appendQueryParams(preformattedUrl, data) : preformattedUrl

	const response = await fetch(url, {
		headers: {
			...HEADERS,
			...headers,
		},
		method,
		body: !isGet && data ? JSON.stringify(data) : body,
	})

	if (response.status < 200 || response.status >= 400) {
		throw new Error(response.status.toString())
	}

	const responseData: S = await response[dataType]()

	return (typeof responseFormatter === 'function' ? responseFormatter(responseData) : responseData) as T
}

export default requestFactory()
export { requestFactory }
