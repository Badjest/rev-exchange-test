interface QueryParams {
	[paramKey: string]: string | number | boolean
}

const appendQueryParams = (baseUrl = '', params: QueryParams): string => {
	if (Object.keys(params).length === 0) return baseUrl

	const queryParams = new URLSearchParams()

	Object.entries(params).forEach(([key, value]) => {
		if (value) queryParams.append(key, String(value))
	})

	const query = queryParams.toString()

	return `${baseUrl}${query ? '?' : ''}${query}`
}

export default appendQueryParams
