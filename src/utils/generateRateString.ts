import { RatesType } from 'api/index.d'
import { AccountType } from 'store/mocks/accounts'

export default function generateRateString(fromAccount: AccountType, toAccount: AccountType, rates: RatesType | null) {
	if (!rates) return ''

	return `1${fromAccount.symbol} = ${rates[fromAccount.currency][toAccount.currency]}${toAccount.symbol}`
}
