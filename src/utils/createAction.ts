export default function createAction<T>(type: string) {
	const actionCreator = function(payload?: T) {
		return {
			type,
			payload,
		}
	}

	actionCreator.toString = () => type

	return actionCreator
}
