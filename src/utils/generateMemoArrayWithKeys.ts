import generateRandomString from './generateRandomString'

const memoArrays = new Map()

export default function generateMemoArrayWithKeys(count: number): string[] {
	const memoArray = memoArrays.get(count)

	if (memoArray) {
		return memoArray
	}

	const newArray = new Array(count).fill(null).map(() => generateRandomString())

	memoArrays.set(count, newArray)

	return newArray
}
