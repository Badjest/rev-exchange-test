import createAction from 'utils/createAction'

const ACTION_NAME = 'ACTION'

describe('utils', () => {
	describe('createAction', () => {
		const action = createAction(ACTION_NAME)
		const payload = 'SOME PAYLOAD'

		it('result -> Function', () => {
			expect(typeof action).toBe('function')
		})

		it('toString() must return action name', () => {
			expect(action.toString()).toBe(ACTION_NAME)
		})

		it('action call -> { type, payload }', () => {
			expect(action()).toMatchObject({
				type: ACTION_NAME,
				payload: undefined,
			})

			expect(action(payload)).toMatchObject({
				type: ACTION_NAME,
				payload,
			})
		})
	})
})
