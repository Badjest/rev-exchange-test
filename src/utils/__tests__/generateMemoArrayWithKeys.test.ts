import generateMemoArrayWithKeys from 'utils/generateMemoArrayWithKeys'

describe('utils', () => {
	describe('generateMemoArrayWithKeys', () => {
		const result = generateMemoArrayWithKeys(3)

		it('return array', () => {
			expect(Array.isArray(result)).toBeTruthy()
		})

		it('new call with the same parameter returns the same array', () => {
			expect(result).toBe(generateMemoArrayWithKeys(3))
		})

		it('try with new params', () => {
			expect(generateMemoArrayWithKeys(5)).toBe(generateMemoArrayWithKeys(5))
			expect(generateMemoArrayWithKeys(4)).not.toBe(generateMemoArrayWithKeys(5))
		})
	})
})
