import generateRandomString from 'utils/generateRandomString'

describe('utils', () => {
	describe('generateRandomString', () => {
		it('expected string', () => {
			expect(typeof generateRandomString()).toBe('string')
		})

		it('expected different strings', () => {
			expect(generateRandomString()).not.toBe(generateRandomString())
		})
	})
})
