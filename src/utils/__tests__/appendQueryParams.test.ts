import appendQueryParams from 'utils/appendQueryParams'

describe('utils', () => {
	describe('appendQueryParams', () => {
		it('simple check', () => {
			const data = {
				one: '1',
				two: '2',
			}

			expect(appendQueryParams('https://test.com', data)).toBe('https://test.com?one=1&two=2')
			expect(appendQueryParams('', data)).toBe('?one=1&two=2')
			expect(appendQueryParams('', {})).toBe('')
		})
	})
})
