import generateRateString from 'utils/generateRateString'
import { CURRENCY } from 'constants/currency'
import { AccountType } from 'store/mocks/accounts'
import { RatesType } from 'api'

const USD_ACCOUNT: AccountType = {
	id: '1234',
	value: 150.44,
	currency: 'USD',
	symbol: CURRENCY.USD,
}

const EUR_ACCOUNT: AccountType = {
	id: '5678',
	value: 324,
	currency: 'EUR',
	symbol: CURRENCY.EUR,
}

const RATES = {
	USD: {
		EUR: 0.956843,
	},
	EUR: {
		USD: 1.123843,
	},
} as RatesType

const EXPECTED_STRING_FROM_USD = `1${USD_ACCOUNT.symbol} = ${RATES[USD_ACCOUNT.currency][EUR_ACCOUNT.currency]}${
	EUR_ACCOUNT.symbol
}`
const EXPECTED_STRING_FROM_EUR = `1${EUR_ACCOUNT.symbol} = ${RATES[EUR_ACCOUNT.currency][USD_ACCOUNT.currency]}${
	USD_ACCOUNT.symbol
}`

describe('utils', () => {
	describe('generateRateString', () => {
		it('without rates param', () => {
			const result = generateRateString(USD_ACCOUNT, EUR_ACCOUNT, null)

			expect(typeof result).toBe('string')
			expect(result).toBe('')
		})

		it('with rates param, USD -> EUR', () => {
			const result = generateRateString(USD_ACCOUNT, EUR_ACCOUNT, RATES)

			expect(typeof result).toBe('string')
			expect(result.includes(String(RATES.USD.EUR))).toBeTruthy()
			expect(result.includes(USD_ACCOUNT.symbol)).toBeTruthy()
			expect(result).toBe(EXPECTED_STRING_FROM_USD)
		})

		it('with rates param, EUR -> USD', () => {
			const result = generateRateString(EUR_ACCOUNT, USD_ACCOUNT, RATES)

			expect(typeof result).toBe('string')
			expect(result.includes(String(RATES.EUR.USD))).toBeTruthy()
			expect(result.includes(EUR_ACCOUNT.symbol)).toBeTruthy()
			expect(result).toBe(EXPECTED_STRING_FROM_EUR)
		})
	})
})
