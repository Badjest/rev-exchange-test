import request, { requestFactory, HEADERS } from 'utils/request'
import { FetchMock } from 'jest-fetch-mock'

const fetchMock = fetch as FetchMock

const RESPONSE_DATA = { data: '12345' }
const FORMAT_DATA = { otherData: '6789' }
const DOMAIN = 'http://test.com'
const REQUEST_URL = '/test'

beforeEach(() => {
	fetchMock.resetMocks()
})

describe('utils', () => {
	describe('request && requestFactory', () => {
		it('just call request', async () => {
			fetchMock.mockResponseOnce(JSON.stringify(RESPONSE_DATA))

			const result = await request(REQUEST_URL)
			const url: string = fetchMock.mock.calls[0][0]

			expect(fetchMock.mock.calls.length).toEqual(1)
			expect(url).toEqual(REQUEST_URL)
			expect(fetchMock.mock.calls[0][1].method).toEqual('GET')
			expect(fetchMock.mock.calls[0][1].headers).toMatchObject(HEADERS)
			expect(result).toMatchObject(RESPONSE_DATA)
		})

		it('requestFactory', async () => {
			fetchMock.mockResponseOnce('1')

			const myRequest = requestFactory(DOMAIN)

			await myRequest(REQUEST_URL)

			const url: string = fetchMock.mock.calls[0][0]

			expect(fetchMock.mock.calls.length).toEqual(1)
			expect(url).toEqual(`${DOMAIN}${REQUEST_URL}`)
		})

		it('POST request with formatter', async () => {
			fetchMock.mockResponseOnce(JSON.stringify(RESPONSE_DATA))

			const result = await request(REQUEST_URL, {
				method: 'POST',
				responseFormatter: (data) => ({
					...data,
					...FORMAT_DATA,
				}),
			})

			const url: string = fetchMock.mock.calls[0][0]

			expect(url).toEqual(REQUEST_URL)
			expect(fetchMock.mock.calls[0][1].method).toEqual('POST')
			expect(result).toMatchObject({ ...RESPONSE_DATA, ...FORMAT_DATA })
		})

		it('POST request with error status', async () => {
			fetchMock.mockResponseOnce('1', { status: 404 })

			try {
				await request(REQUEST_URL, {
					method: 'POST',
				})
			} catch (e) {
				expect(e.message).toBe('404')
			}

			expect(fetchMock.mock.calls.length).toEqual(1)
		})
	})
})
