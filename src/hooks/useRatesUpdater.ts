import { useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { UPDATE_TIMEOUT, TIMEOUT_AFTER_CRASH } from 'constants/time'
import { STATUSES } from 'constants/values'
import { setRates } from 'store/actions'
import { selectRates } from 'store/selectors'
import { getRates } from 'api/currencyRequest'
import useNotification from './useNotification'

const useRatesUpdater = () => {
	const rates = useSelector(selectRates)
	const dispatch = useDispatch()
	const timeout = useRef<number>()
	const showNotification = useNotification()

	const updateRates = async () => {
		try {
			const rates = await getRates()

			dispatch(setRates(rates))

			timeout.current = setTimeout(updateRates, UPDATE_TIMEOUT)
		} catch (e) {
			console.error(e)

			showNotification('warning', STATUSES.WRONG_RATES)

			timeout.current = setTimeout(updateRates, TIMEOUT_AFTER_CRASH)
		}
	}

	const cancelUpdater = () => clearTimeout(timeout.current)

	useEffect(() => {
		updateRates()

		return () => cancelUpdater()
	}, [])

	return { rates, cancelUpdater }
}

export default useRatesUpdater
