import { useEffect, useState } from 'react'
import { AccountType } from 'store/mocks/accounts'

const useExchangeChecker = (fromAcount: AccountType, fromValue: number) => {
	const [error, setError] = useState('')
	const [canExchange, setCanExchange] = useState(false)

	useEffect(() => {
		if (fromAcount.value < fromValue) {
			return setError('Sorry, insufficient funds')
		}

		if (error) {
			setError('')
		}
	}, [fromAcount, fromValue])

	useEffect(() => {
		setCanExchange(Boolean(fromValue) && !error)
	}, [error, fromValue])

	return { error, canExchange }
}

export default useExchangeChecker
