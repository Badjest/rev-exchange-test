import { ReactNode } from 'react'
import { useDispatch } from 'react-redux'
import { SnackType } from 'components/UI/Snackbar/Snackbar.d'
import { showNotification as showNotificationAction } from 'store/actions'

export default function useNotification() {
	const dispatch = useDispatch()

	const showNotification = (type: SnackType, content: ReactNode) => dispatch(showNotificationAction({ type, content }))

	return showNotification
}
