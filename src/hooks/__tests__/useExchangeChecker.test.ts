import { renderHook } from '@testing-library/react-hooks'
import { useExchangeChecker } from 'hooks'
import { AccountType } from 'store/mocks/accounts'
import { CURRENCY } from 'constants/currency'

const USD_ACCOUNT: AccountType = {
	id: '1234',
	value: 150.44,
	currency: 'USD',
	symbol: CURRENCY.USD,
}

const VALUE = 120.2

describe('hooks', () => {
	describe('useExchangeChecker', () => {
		it('with correct params', () => {
			const { result } = renderHook(() => useExchangeChecker(USD_ACCOUNT, VALUE))
			const { error, canExchange } = result.current

			expect(typeof error).toBe('string')
			expect(canExchange).toBeTruthy()
		})

		it('with rerender', () => {
			const hook = renderHook<{ fromAccount: AccountType; fromValue: number }, { canExchange: boolean; error: string }>(
				({ fromAccount, fromValue }) => useExchangeChecker(fromAccount, fromValue),
				{
					initialProps: {
						fromAccount: USD_ACCOUNT,
						fromValue: VALUE,
					},
				},
			)

			expect(hook.result.current.canExchange).toBeTruthy()

			const NEW_BIG_VALUE = 1000

			hook.rerender({ fromAccount: USD_ACCOUNT, fromValue: NEW_BIG_VALUE })

			expect(hook.result.current.canExchange).toBeFalsy()
			expect(hook.result.current.error).toBe('Sorry, insufficient funds')
		})

		it('with equal params', () => {
			const { result } = renderHook(() => useExchangeChecker(USD_ACCOUNT, USD_ACCOUNT.value))
			const { error, canExchange } = result.current

			expect(typeof error).toBe('string')
			expect(canExchange).toBeTruthy()
		})
	})
})
