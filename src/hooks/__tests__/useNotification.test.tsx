import React from 'react'
import { renderHook } from '@testing-library/react-hooks'
import { useNotification } from 'hooks'
import { Provider } from 'react-redux'
import configureMockStore from 'redux-mock-store'
import { showNotification } from 'store/actions'
import { SnackType } from 'components/UI/Snackbar/Snackbar.d'

const mockStore = configureMockStore([])
const store = mockStore({
	notifications: [],
})

const VARIANTS = [['done', 'done'], ['warning', 'error']]

describe('hooks', () => {
	describe('useNotification', () => {
		it('call showNotififcation and check store', async () => {
			const { result } = renderHook(() => useNotification(), {
				wrapper: ({ children }) => <Provider store={store}>{children}</Provider>,
			})

			for (const [type, content] of VARIANTS) {
				result.current(type as SnackType, content)

				const itemIsInActions = store
					.getActions()
					.some(
						({ type: actionType, payload }) =>
							actionType === showNotification.toString() && payload.type === type && payload.content === content,
					)

				expect(itemIsInActions).toBeTruthy()
			}
		})
	})
})
