import React from 'react'
import { renderHook } from '@testing-library/react-hooks'
import { useRatesUpdater } from 'hooks'
import { Provider } from 'react-redux'
import { FetchMock } from 'jest-fetch-mock'
import { UPDATE_TIMEOUT } from 'constants/time'
import { RATES_RESPONSE_DATA, RATES_RESPONSE_DATA_2, RATES_AFTER_FORMAT, RATES_AFTER_FORMAT_2 } from 'test/mockData'
import store from 'store/index'

const fetchMock = fetch as FetchMock

jest.useFakeTimers()

beforeEach(() => {
	fetchMock.resetMocks()
})

describe('hooks', () => {
	describe('useRatesUpdater', () => {
		it('call with two updates', async () => {
			fetchMock.mockResponses(
				[JSON.stringify(RATES_RESPONSE_DATA), { status: 200 }],
				[JSON.stringify(RATES_RESPONSE_DATA_2), { status: 200 }],
			)

			const { result, waitForNextUpdate } = renderHook(() => useRatesUpdater(), {
				wrapper: ({ children }) => <Provider store={store}>{children}</Provider>,
			})

			await waitForNextUpdate()

			expect(result.current.rates).toMatchObject(RATES_AFTER_FORMAT)

			jest.advanceTimersByTime(UPDATE_TIMEOUT)

			await waitForNextUpdate()

			expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), UPDATE_TIMEOUT)
			expect(result.current.rates).toMatchObject(RATES_AFTER_FORMAT_2)

			result.current.cancelUpdater()

			jest.advanceTimersByTime(2000)

			expect(jest.getTimerCount()).toBe(0)
		})
	})
})
