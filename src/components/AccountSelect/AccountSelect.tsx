import React, { memo } from 'react'

import { AccountType } from 'store/mocks/accounts'
import { SmoothUpdater } from 'components'
import { MoneyInput, Informer, Dots } from 'components/UI'
import { IMoneyInput } from 'components/UI/Input/Input.d'

const Label = memo(({ currency, from }: { currency: string; from?: boolean }) => (
	<>
		{from ? 'From' : 'To'}{' '}
		<strong>
			<SmoothUpdater duration={0.3}>{currency}</SmoothUpdater>
		</strong>{' '}
		account:
	</>
))

interface IAccountSelect {
	account: AccountType
	accountsCount: number
	onChange: (currentIndex: number) => void
	inputProps?: IMoneyInput
	from?: boolean
	disabled?: boolean
}

const AccountSelect: React.FC<IAccountSelect> = ({
	account = {},
	disabled,
	from,
	accountsCount,
	onChange,
	inputProps,
}) => {
	const { currency = '', value, symbol } = account
	const youHave = `${value}${symbol}`

	return (
		<>
			<MoneyInput disabled={disabled} label={<Label currency={currency} from={from} />} {...inputProps} />
			<Informer>
				You have: <SmoothUpdater duration={0.3}>{youHave}</SmoothUpdater>
			</Informer>
			<Dots disabled={disabled} onClick={onChange} count={accountsCount} />
		</>
	)
}

export default memo(AccountSelect)
