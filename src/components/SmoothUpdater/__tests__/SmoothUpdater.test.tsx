import React from 'react'
import renderer from 'react-test-renderer'
import { render, act, fireEvent } from '@testing-library/react'

import SmoothUpdater from 'components/SmoothUpdater'

const VALUE = 'value'
const NEW_VALUE = 'newvalue'

describe('components', () => {
	describe('SmoothUpdater', () => {
		it('render', () => {
			const { container } = render(<SmoothUpdater>{VALUE}</SmoothUpdater>)
			const inner = container.firstChild as HTMLSpanElement

			expect(inner).toBeDefined()
			expect(inner.innerHTML).toBe(VALUE)
		})

		it(`component doesn't change the child immediately`, async () => {
			const { container, rerender } = render(<SmoothUpdater>{VALUE}</SmoothUpdater>)
			const inner = container.firstChild as HTMLSpanElement

			await act(async () => {
				await rerender(<SmoothUpdater children={NEW_VALUE} />)

				expect(inner.innerHTML).toBe(VALUE)

				fireEvent.transitionEnd(inner)
			})

			expect(inner.innerHTML).toBe(NEW_VALUE)
		})

		it('snapshot', () => {
			const tree = renderer.create(<SmoothUpdater>{VALUE}</SmoothUpdater>).toJSON()

			expect(tree).toMatchSnapshot()
		})
	})
})
