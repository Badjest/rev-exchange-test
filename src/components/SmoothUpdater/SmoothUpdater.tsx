import React, { useEffect, useState, useCallback, memo } from 'react'
import styled from 'styled-components'

const Wrapper = styled.span<{ hide: boolean; duration: number }>`
	opacity: ${({ hide }) => (hide ? 0 : 1)};

	transition: opacity ${({ duration }) => duration}s ease-out;
`

/**
 * This components works correctly only with primitive children,
 * e.g. <SmoothUpdater>{'just string'}</SmoothUpdater>
 */
const SmoothUpdater = ({ children, duration = 0.2 }: { children: string | number; duration?: number }) => {
	const [currentChildren, setNewChildren] = useState<React.ReactNode>(children)
	const [hide, setHide] = useState(false)

	useEffect(() => {
		if (!currentChildren) {
			return setNewChildren(children)
		}

		setHide(true)
	}, [children])

	const onTransitionEnd = useCallback(() => {
		if (hide) {
			setNewChildren(children)
			setHide(false)
		}
	}, [children, hide])

	return (
		<Wrapper duration={duration} onTransitionEnd={onTransitionEnd} hide={hide}>
			{currentChildren}
		</Wrapper>
	)
}

export default memo(SmoothUpdater)
