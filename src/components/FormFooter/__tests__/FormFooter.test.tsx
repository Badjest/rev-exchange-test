import React from 'react'
import renderer from 'react-test-renderer'
import { render } from '@testing-library/react'

import FormFooter from 'components/FormFooter'

const CONTENT = 'content'

describe('components/UI', () => {
	describe('FormFooter', () => {
		it('render', () => {
			const { container } = render(
				<FormFooter canExchange={false} inProccess={false}>
					{CONTENT}
				</FormFooter>,
			)
			const button = container.querySelector('button:disabled') as HTMLButtonElement

			expect(button).toBeDefined()
			expect(button.innerHTML).toBe(CONTENT)
		})

		it('snapshot with false props', () => {
			const tree = renderer
				.create(
					<FormFooter canExchange={false} inProccess={false}>
						{CONTENT}
					</FormFooter>,
				)
				.toJSON()

			expect(tree).toMatchSnapshot()
		})

		it('snapshot with true props', () => {
			const tree = renderer
				.create(
					<FormFooter canExchange={true} inProccess={true}>
						{CONTENT}
					</FormFooter>,
				)
				.toJSON()

			expect(tree).toMatchSnapshot()
		})
	})
})
