import React from 'react'
import styled from 'styled-components'
import { Button, Loader } from 'components/UI'
import { StyledCenter } from 'components/UI/AdditionalUI'

const StyledLoader = styled(Loader)`
	position: absolute;
	top: 0;
	right: 0;
`

const FormFooter: React.FC<{ canExchange: boolean; inProccess: boolean }> = ({ children, canExchange, inProccess }) => (
	<StyledCenter>
		<Button disabled={!canExchange || inProccess} type="submit">
			{children}
		</Button>
		{inProccess && <StyledLoader />}
	</StyledCenter>
)

export default FormFooter
