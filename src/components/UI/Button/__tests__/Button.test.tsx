import React from 'react'
import renderer from 'react-test-renderer'
import { render, fireEvent } from '@testing-library/react'

import { Button } from 'components/UI'

const BUTTON_CONTENT = 'Hello'

describe('components/UI', () => {
	describe('Button', () => {
		const onClick = jest.fn()

		it('render', () => {
			const { container } = render(<Button onClick={onClick}>{BUTTON_CONTENT}</Button>)
			const button = container.querySelector('button') as HTMLButtonElement

			expect(button.innerHTML).toBe(BUTTON_CONTENT)

			fireEvent.click(button)

			expect(onClick).toHaveBeenCalledTimes(1)
		})

		it('snapshot', () => {
			const tree = renderer.create(<Button>{BUTTON_CONTENT}</Button>).toJSON()

			expect(tree).toMatchSnapshot()
		})
	})
})
