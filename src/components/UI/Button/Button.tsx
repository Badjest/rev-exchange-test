import styled from 'styled-components'

const Button = styled.button`
	display: block;
	padding: 0.7rem 2rem;
	border: none;
	margin: 1.5rem auto 0 auto;
	background: white;
	border-radius: 0.5rem;
	box-shadow: 0 0 1.5rem 0 rgba(0, 0, 0, 0.2);
	color: #2196f3;
	cursor: pointer;
	font-size: 1.5rem;
	letter-spacing: 0.1rem;
	outline: none;
	transition: background 0.2s, color 0.2s, transform 0.1s;

	:hover {
		background: #1976d2;
		color: white;
	}

	:active {
		transform: scale(0.95);
	}

	:disabled {
		background: grey;
		color: white;
		cursor: not-allowed;
		opacity: 0.5;
	}
`

export default Button
