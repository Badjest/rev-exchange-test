import React from 'react'
import renderer from 'react-test-renderer'
import { render } from '@testing-library/react'

import { Snackbar } from 'components/UI'
import { colorMap } from '../Snackbar'

const CONTENT = 'content'

describe('components/UI', () => {
	describe('Snackbar', () => {
		it('render', () => {
			const { container, rerender } = render(<Snackbar snackType="done">{CONTENT}</Snackbar>)
			const snack = container.firstChild as HTMLElement

			expect(snack).toBeDefined()
			expect(snack.innerHTML).toBe(CONTENT)
			expect(snack).toHaveStyleRule('background-color', colorMap['done'])

			rerender(<Snackbar snackType="warning">{CONTENT}</Snackbar>)

			expect(snack).toHaveStyleRule('background-color', colorMap['warning'])
		})

		it('snapshot', () => {
			const tree = renderer.create(<Snackbar snackType="done">{CONTENT}</Snackbar>).toJSON()

			expect(tree).toMatchSnapshot()
		})
	})
})
