import { colorMap } from './Snackbar'

export type SnackType = keyof typeof colorMap

export interface ISnackbar {
	snackType: SnackType
}
