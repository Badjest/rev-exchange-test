import styled from 'styled-components'

import { ISnackbar } from './Snackbar.d'

export const colorMap = {
	done: '#43a047',
	warning: '#ffa000',
}

const Snackbar = styled.div<ISnackbar>`
	position: absolute;

	top: 1rem;
	left: 1rem;

	max-width: 100vw;
	padding: 1rem 2rem;
	background-color: ${({ snackType = 'done' }) => colorMap[snackType]};
	border-radius: 0.5rem;
	box-shadow: 0 3px 5px -1px rgba(0, 0, 0, 0.2);
	color: white;
	cursor: pointer;
	white-space: nowrap;
`

export default Snackbar
