import React from 'react'
import renderer from 'react-test-renderer'
import { render, fireEvent } from '@testing-library/react'
import { act } from 'react-dom/test-utils'

import { Dots } from 'components/UI'

const DOTS_COUNT = 3

describe('components/UI', () => {
	describe('Dots', () => {
		const onClick = jest.fn()

		it('render', () => {
			const { container } = render(<Dots onClick={onClick} count={DOTS_COUNT} />)
			const divs = container.querySelectorAll('div')

			expect(divs.length).toBe(DOTS_COUNT + 1)

			for (const div of divs) {
				fireEvent.click(div)
			}

			expect(onClick).toHaveBeenCalledTimes(DOTS_COUNT)
		})

		it('snapshot', () => {
			const tree = renderer.create(<Dots count={DOTS_COUNT} onClick={onClick} />).toJSON()

			expect(tree).toMatchSnapshot()
		})
	})
})
