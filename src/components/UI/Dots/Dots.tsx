import React, { memo, useState, useCallback } from 'react'
import styled from 'styled-components'

import generateMemoArrayWithKeys from 'utils/generateMemoArrayWithKeys'

import { IDots } from './Dots.d'

const Dot = styled.div<{ active?: boolean }>`
	--dot-size: 10px;
	width: var(--dot-size);
	height: var(--dot-size);
	background-color: #000;
	background-color: white;
	border-radius: 50%;
	cursor: pointer;
	opacity: ${({ active }) => (active ? 1 : 0.5)};
	transition: opacity 0.2s;

	& + & {
		margin-left: 0.5rem;
	}

	:hover {
		opacity: 1;
	}
`

const StyledDots = styled.div`
	display: flex;
	justify-content: center;
	margin: 1rem 0;
`

const Dots: React.FC<IDots> = ({ count, onClick, disabled }) => {
	const [activeIndex, setActiveIndex] = useState(0)

	const onClickHandler = useCallback(
		(currentIndex: number) => () => {
			if (disabled) return

			onClick(currentIndex)
			setActiveIndex(currentIndex)
		},
		[disabled],
	)

	return (
		<StyledDots>
			{generateMemoArrayWithKeys(count).map((key, i) => (
				<Dot onClick={onClickHandler(i)} active={i === activeIndex} key={key} />
			))}
		</StyledDots>
	)
}

export default memo(Dots)
