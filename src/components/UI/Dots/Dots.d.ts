export interface IDots {
	count: number
	onClick: (currentIndex: number) => void
	disabled?: boolean
}
