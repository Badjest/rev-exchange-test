import { ChangeEvent } from 'react'

export interface IInput {
	id?: string
	label?: React.ReactNode
	placeholder?: string
	value?: string
	className?: string
	autoFocus?: boolean
	error?: string
	disabled?: boolean
	onChange?: (e: ChangeEvent<HTMLInputElement>) => void
	onFocus?: (e: Event<HTMLInputElement>) => void
	[key: string]: any
}

export interface IMoneyInput extends Omit<IInput, 'onChange'> {
	onChange?: (value: string) => void
}
