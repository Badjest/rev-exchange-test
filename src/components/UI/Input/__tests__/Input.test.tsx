import React from 'react'
import renderer from 'react-test-renderer'
import { render } from '@testing-library/react'

import { Input } from 'components/UI'

const ERROR_TEXT = 'error'

describe('components/UI', () => {
	describe('Input', () => {
		it('render', () => {
			const { container } = render(<Input label="label" />)

			expect(container.querySelector('input')).toBeDefined()
			expect(container.querySelector('label')).toBeDefined()
		})

		it('autoFocus && error test', () => {
			const onFocus = jest.fn()
			const { container, rerender } = render(<Input autoFocus onFocus={onFocus} />)

			expect(onFocus).toHaveBeenCalledTimes(1)

			rerender(<Input autoFocus error={ERROR_TEXT} />)

			const errorElement = container.querySelector('input ~ div') as HTMLDivElement

			expect(errorElement).toBeDefined()
			expect(errorElement.innerHTML).toBe(ERROR_TEXT)
		})

		it('snapshot', () => {
			const tree = renderer.create(<Input id="test-id" />).toJSON()

			expect(tree).toMatchSnapshot()
		})
	})
})
