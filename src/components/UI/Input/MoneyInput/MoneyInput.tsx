import React, { ChangeEvent, useCallback } from 'react'

import { replaceCommaToDot, formatChecker } from './MoneyInput.utils'
import Input from '../Input'
import { IMoneyInput } from '../Input.d'

const MoneyInput: React.FC<IMoneyInput> = ({ onChange, ...restProps }) => {
	const onChangeHandler = useCallback((e: ChangeEvent<HTMLInputElement>) => {
		const changedValue = replaceCommaToDot(e.target.value)
		const isCorrectValue = formatChecker(changedValue)

		if (onChange && isCorrectValue) {
			onChange(changedValue)
		}
	}, [])

	return <Input onChange={onChangeHandler} {...restProps} />
}

export default MoneyInput
