import React from 'react'
import { render, fireEvent } from '@testing-library/react'

import { MoneyInput } from 'components/UI'
import { formatChecker } from '../MoneyInput.utils'

const RIGHT_VALUES = ['1.12', '2323.33', '15', '22.01', '0']
const WRONG_VALUES = ['null', 'hello', '00', '33..', '44.2324']

describe('components/UI/Input', () => {
	describe('MoneyInput', () => {
		it('render', () => {
			const onChange = jest.fn()
			const { container } = render(<MoneyInput onChange={onChange} />)
			const input = container.querySelector('input') as HTMLInputElement

			expect(input).toBeDefined()

			for (const rightValue of RIGHT_VALUES) {
				fireEvent.change(input, { target: { value: rightValue } })
			}

			expect(onChange).toHaveBeenCalledTimes(RIGHT_VALUES.length)
			onChange.mockClear()

			for (const wrongValue of WRONG_VALUES) {
				fireEvent.change(input, { target: { value: wrongValue } })
			}

			expect(onChange).toHaveBeenCalledTimes(0)
		})

		it('formatChecker test', () => {
			for (const rightValue of RIGHT_VALUES) {
				expect(formatChecker(rightValue)).toBeTruthy()
			}

			for (const wrongValue of WRONG_VALUES) {
				expect(formatChecker(wrongValue)).toBeFalsy()
			}
		})
	})
})
