import { MAX_VALUE } from 'constants/values'

export const replaceCommaToDot = (value: string) => value.replace(',', '.')

export const formatChecker = (value: string) => {
	const regexp = /^\d+(\.\d{0,2})?$/

	return (regexp.test(value) || value === '') && value !== '00' && Number(value) <= MAX_VALUE
}
