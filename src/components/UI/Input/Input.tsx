import React, { useMemo, memo, useRef, useEffect } from 'react'
import styled from 'styled-components'

import generateRandomString from 'utils/generateRandomString'

import { IInput } from './Input.d'

const InputBlock = styled.div`
	--error-size: 0.8rem;
	position: relative;

	padding-bottom: calc(var(--error-size) + 0.3rem);
	margin-bottom: 0.3rem;
`

const StyledInput = styled.input<{ error?: boolean }>`
	--placeholder-color: #1976d2;
	--underline-color: ${({ error }) => (error ? '#ff80ab' : '#1976d2')};
	width: 100%;
	height: 4rem;
	padding: 2px;
	border: 0;
	border-bottom: 2px solid var(--underline-color);
	background: transparent;
	border-radius: 0;
	color: #fff;
	font-size: 2rem;
	line-height: 35px;
	outline: none;
	transition: border 0.2s;

	::placeholder,
	::-webkit-input-placeholder {
		color: var(--placeholder-color);
		letter-spacing: 0.35rem;
	}

	:disabled {
		opacity: 0.7;
	}

	&:focus {
		--underline-color: #bbdefb;
	}
`

const StyledLabel = styled.label`
	display: block;
	margin-bottom: -0.2rem;
	color: white;
`

const ErrorText = styled.div`
	position: absolute;
	bottom: 0;
	left: 0;

	width: 100%;
	padding: 0.2rem;
	background-color: white;
	border-bottom-left-radius: 0.3rem;
	border-bottom-right-radius: 0.3rem;
	color: #d32f2f;
	font-size: var(--error-size);
`

const Input: React.FC<IInput> = ({ id, label, autoFocus, error, ...restProps }) => {
	const currnetId = useMemo(() => id || generateRandomString(), [id])
	const inputRef = useRef() as React.MutableRefObject<HTMLInputElement>

	useEffect(() => {
		if (autoFocus) {
			inputRef.current.focus()
		}
	}, [])

	useEffect(() => {
		if (restProps.disabled) {
			inputRef.current.blur()
		}
	}, [restProps.disabled])

	return (
		<InputBlock>
			{label && <StyledLabel htmlFor={currnetId}>{label}</StyledLabel>}

			<StyledInput error={!!error} ref={inputRef} id={currnetId} {...restProps} />
			{error && <ErrorText>{error}</ErrorText>}
		</InputBlock>
	)
}

export default memo(Input)
