import styled from 'styled-components'

export const StyledCenter = styled.div`
	position: relative;
	text-align: center;
`

export const Informer = styled.div`
	color: white;
`

export const Arrow = styled.div`
	width: 2rem;
	height: 0.7rem;
	margin: 1rem auto;
	background: white;
	clip-path: polygon(0 0, 0 10%, 50% 100%, 100% 10%, 100% 0);
	opacity: 0.5;
`
