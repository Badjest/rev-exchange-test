import React from 'react'
import renderer from 'react-test-renderer'
import { render } from '@testing-library/react'

import { Chip } from 'components/UI'

describe('components/UI', () => {
	describe('Chip', () => {
		it('render', () => {
			const { container } = render(<Chip />)

			expect(container.querySelectorAll('div').length).toBe(1)
		})

		it('snapshot', () => {
			const tree = renderer.create(<Chip />).toJSON()

			expect(tree).toMatchSnapshot()
		})
	})
})
