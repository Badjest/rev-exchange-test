import styled from 'styled-components'

const Chip = styled.div`
	display: inline-flex;
	padding: 0.5rem 1rem;
	border: none;
	background-color: #e0e0e0;
	border-radius: 2rem;
	color: black;
	white-space: nowrap;
`

export default Chip
