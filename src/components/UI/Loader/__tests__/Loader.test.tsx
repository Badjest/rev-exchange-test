import React from 'react'
import renderer from 'react-test-renderer'
import { render } from '@testing-library/react'

import { Loader } from 'components/UI'

const TEST_CLASSNAME = 'test-classname'

describe('components/UI', () => {
	describe('Loader', () => {
		it('render', () => {
			const { container, rerender } = render(<Loader />)

			expect(container.querySelectorAll('div').length).toBe(5)

			rerender(<Loader className={TEST_CLASSNAME} />)

			expect(container.querySelector(`.${TEST_CLASSNAME}`)).toBeDefined()
		})

		it('snapshot', () => {
			const tree = renderer.create(<Loader />).toJSON()

			expect(tree).toMatchSnapshot()
		})
	})
})
