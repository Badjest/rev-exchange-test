import React from 'react'
import styled, { keyframes } from 'styled-components'

const rotate = keyframes`
	from {
		transform: rotate(0deg);
	}

	to {
		transform: rotate(360deg);
	}
`

const RingChild = styled.div`
	position: absolute;

	display: block;
	width: var(--ring-size);
	height: var(--ring-size);
	border: 0.5rem solid #fff;
	border-color: #fff transparent transparent transparent;
	animation: ${rotate} 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
	border-radius: 50%;
`

const Ring = styled.div`
	--ring-size: 3rem;
	position: relative;

	display: inline-block;
	width: var(--ring-size);
	height: var(--ring-size);

	${RingChild}:nth-child(1) {
		animation-delay: -0.45s;
	}

	${RingChild}:nth-child(2) {
		animation-delay: -0.3s;
	}

	${RingChild}:nth-child(3) {
		animation-delay: -0.15s;
	}
`

const Loader: React.FC<{ className?: string }> = ({ className }) => (
	<Ring className={className}>
		<RingChild />
		<RingChild />
		<RingChild />
		<RingChild />
	</Ring>
)

export default Loader
