import React from 'react'
import { createPortal } from 'react-dom'

const modalRoot = typeof document !== 'undefined' ? document.getElementById('modal') : null
const IS_TEST = process.env.NODE_ENV === 'test'

const ModalPortal: React.FC = ({ children }) => {
	if (modalRoot) return createPortal(children, modalRoot)

	return IS_TEST ? <>{children}</> : null
}

export default ModalPortal
