export const RATES_RESPONSE_DATA = {
	base: 'USD',
	rates: {
		EUR: 0.903778,
		GBP: 0.776223,
		USD: 1,
	},
}

export const RATES_RESPONSE_DATA_2 = {
	base: 'USD',
	rates: {
		EUR: 0.953778,
		GBP: 0.766223,
		USD: 1,
	},
}

export const RATES_AFTER_FORMAT = {
	USD: { EUR: 0.903778, GBP: 0.776223, USD: 1 },
	EUR: { USD: 1.106466, GBP: 0.858864 },
	GBP: { USD: 1.28829, EUR: 1.164328 },
}

export const RATES_AFTER_FORMAT_2 = {
	USD: { EUR: 0.953778, GBP: 0.766223, USD: 1 },
	EUR: { USD: 1.048462, GBP: 0.803356 },
	GBP: { USD: 1.305103, EUR: 1.244779 },
}
