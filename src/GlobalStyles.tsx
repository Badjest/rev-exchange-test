import { createGlobalStyle } from 'styled-components'

const GlobalStyles = createGlobalStyle`
	* {
		box-sizing: border-box;
	}

	html {
		font-size: 1vw;
	}

	html,
	body,
	#root {
		width: 100%;
		height: 100%;
		padding: 0;
		margin: 0;
	}

	#modal {
		position: fixed;
		top: 0;
		left: 0;
	}

	body {
		font-family: system-ui, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Droid Sans, Helvetica Neue;
	}

	@media (max-width: 1024px) {
		html {
			font-size: 1.5vw;
		}
	}

	@media (max-width: 600px) {
		html {
			font-size: 4vw;
		}
	}
`

export default GlobalStyles
