export const formatMoneyValue = (value: number, digits = 2, toNumber = false): string | number => {
	const newValue = value.toFixed(digits + 4).slice(0, -4)

	return toNumber ? Number(newValue) : newValue
}
