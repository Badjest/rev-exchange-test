import React, { useState, FormEvent, useEffect, useCallback, useMemo } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import styled from 'styled-components'

import { selectAccounts } from 'store/selectors'
import { exchangeAndSave } from 'store/actions'
import { AccountsType } from 'store/mocks/accounts'

import generateRateString from 'utils/generateRateString'

import { AccountSelect, FormFooter, SmoothUpdater } from 'components'
import { Chip, StyledCenter, Arrow } from 'components/UI'

import { useRatesUpdater, useExchangeChecker, useNotification } from 'hooks'

import { formatMoneyValue } from './ExchangeWidget.utils'
import { STATUSES } from 'constants/values'

const StyledWidgetForm = styled.form`
	padding: 2rem 0;
`

const Center = styled(StyledCenter)`
	margin-bottom: 1rem;
`

const ExchangeWidget: React.FC = () => {
	const dispatch = useDispatch()
	const accounts = useSelector(selectAccounts)
	const [restAccounts, setRestAccounts] = useState<AccountsType>([])
	const [fromActiveIndex, setFromActiveIndex] = useState(0)
	const [toActiveIndex, setToActiveIndex] = useState(0)
	const [fromValue, setFromValue] = useState('')
	const [toValue, setToValue] = useState('')
	const [inProccess, setInProccess] = useState(false)
	const [inputInFocus, setInputInFocus] = useState<'from' | 'to'>('from')

	const { rates } = useRatesUpdater()
	const showNotification = useNotification()

	useEffect(() => {
		setRestAccounts(accounts.slice(fromActiveIndex + 1))
	}, [])

	useEffect(() => {
		setRestAccounts(accounts.filter((el) => accounts[fromActiveIndex].id !== el.id))
	}, [fromActiveIndex])

	const clearForm = () => {
		setFromValue('')
		setToValue('')
	}

	const fromAccount = accounts[fromActiveIndex]
	const toAccount = restAccounts[toActiveIndex]

	const onSubmitHandler = async (e: FormEvent<EventTarget>) => {
		e.preventDefault()
		setInProccess(true)

		const changedAccounts = accounts.map((el) => {
			if (el.id === fromAccount.id) {
				el.value = formatMoneyValue(el.value - Number(fromValue), 2, true) as number
			} else if (el.id === toAccount.id) {
				el.value = formatMoneyValue(el.value + Number(toValue), 2, true) as number
			}

			return el
		})

		const result = await dispatch(exchangeAndSave(changedAccounts))

		if (result) {
			showNotification('done', STATUSES.DONE)
		} else {
			showNotification('warning', STATUSES.WRONG)
		}
		setInProccess(false)
		clearForm()
	}

	const { error, canExchange } = useExchangeChecker(fromAccount, Number(fromValue))

	// Main logic when input changing
	useEffect(() => {
		if (!rates || !toAccount) {
			return
		}

		if (!fromValue && inputInFocus === 'from') {
			return setToValue('')
		}

		if (!toValue && inputInFocus === 'to') {
			return setFromValue('')
		}

		const { currency: fromCurrency } = fromAccount
		const { currency: toCurrency } = toAccount
		const rate = inputInFocus === 'to' ? rates[toCurrency][fromCurrency] : rates[fromCurrency][toCurrency]
		const currentValue = Number(inputInFocus === 'to' ? toValue : fromValue) * rate

		if (inputInFocus === 'from') {
			setToValue(formatMoneyValue(currentValue) as string)
		} else {
			setFromValue(formatMoneyValue(currentValue) as string)
		}
	}, [fromValue, toValue, fromAccount, toAccount, rates, error])

	// Change accounts handlers
	const onChangeFromAccount = useCallback((currentIndex: number) => setFromActiveIndex(currentIndex), [])
	const onChangeToAccount = useCallback((currentIndex: number) => setToActiveIndex(currentIndex), [])

	// onFocus for money inputs
	const onFocusHandler = useCallback((name: 'from' | 'to') => () => setInputInFocus(name), [])

	// onChange money inputs handlers
	const onChangeFromAccountInput = useCallback(
		(value: string) => {
			setFromValue(value)
		},
		[fromAccount, toAccount],
	)

	const onChangeToAccountInput = useCallback(
		(value: string) => {
			setToValue(value)
		},
		[fromAccount, toAccount],
	)

	const currentRate = useMemo(() => generateRateString(fromAccount, toAccount, rates), [fromAccount, toAccount, rates])

	return (
		<StyledWidgetForm autoComplete="off" onSubmit={onSubmitHandler}>
			<Center>
				<Chip>
					<SmoothUpdater>{currentRate}</SmoothUpdater>
				</Chip>
			</Center>
			<AccountSelect
				onChange={onChangeFromAccount}
				account={fromAccount}
				accountsCount={accounts.length}
				disabled={inProccess}
				from
				inputProps={{
					placeholder: 'Need to Send',
					autoFocus: true,
					onChange: onChangeFromAccountInput,
					value: fromValue,
					error,
					onFocus: onFocusHandler('from'),
				}}
			/>
			<Arrow />
			<AccountSelect
				onChange={onChangeToAccount}
				account={toAccount}
				accountsCount={restAccounts.length}
				disabled={inProccess}
				inputProps={{
					placeholder: 'Need to Get',
					value: toValue,
					onFocus: onFocusHandler('to'),
					onChange: onChangeToAccountInput,
				}}
			/>
			<FormFooter canExchange={!!rates && canExchange} inProccess={inProccess}>
				Exchange
			</FormFooter>
		</StyledWidgetForm>
	)
}

export default ExchangeWidget
