import React from 'react'
import { FetchMock } from 'jest-fetch-mock'
import { Provider } from 'react-redux'
import { render } from '@testing-library/react'
import configureMockStore from 'redux-mock-store'
import ExchangeWidget from 'containers/ExchangeWidget'

import { formatMoneyValue } from '../ExchangeWidget.utils'
import { initialAccounts } from 'store/mocks/accounts'
import { RATES_RESPONSE_DATA } from 'test/mockData'

const mockStore = configureMockStore([])
const store = mockStore({
	notifications: [],
	accounts: initialAccounts,
	rates: null,
})

const fetchMock = fetch as FetchMock

beforeEach(() => {
	fetchMock.resetMocks()
})

describe('containers', () => {
	describe('ExchangeWidget', () => {
		it('utils', () => {
			const testData = [
				{ value: 12, expectedValue: '12.00' },
				{ value: 23423423, expectedValue: '23423423.00' },
				{ value: 55, expectedValue: '55.00' },
				{ value: 133.123123, expectedValue: '133.12' },
				{ value: 0, expectedValue: '0.00' },
			]

			for (const { value, expectedValue } of testData) {
				const stringResult = formatMoneyValue(value) as string
				const numberResult = formatMoneyValue(value, 2, true) as number

				expect(typeof stringResult).toBe('string')
				expect(stringResult.split('.')[1].length).toBe(2)
				expect(stringResult).toBe(expectedValue)

				expect(typeof numberResult).toBe('number')
				expect(numberResult).toBe(Number(expectedValue))
			}
		})

		it('render with fake store and check important elements', () => {
			fetchMock.mockResponse(JSON.stringify(RATES_RESPONSE_DATA))

			const { container } = render(
				<Provider store={store}>
					<ExchangeWidget />
				</Provider>,
			)

			const form = container.querySelector('form') as HTMLFormElement
			const button = form.querySelector('button') as HTMLButtonElement
			const inputs = form.querySelectorAll('input')

			expect(form).toBeDefined()
			expect(button.getAttribute('disabled')).toBeDefined()
			expect(inputs.length).toBe(2)
		})
	})
})
