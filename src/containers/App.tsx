import React from 'react'
import styled from 'styled-components'
import ExchangeWidget from 'containers/ExchangeWidget'

const Container = styled.main`
	display: flex;
	width: 100%;
	min-height: 100%;
	align-items: center;
	justify-content: center;
	background-color: #2196f3;
`

const App: React.FC = () => (
	<Container>
		<ExchangeWidget />
	</Container>
)

export default App
