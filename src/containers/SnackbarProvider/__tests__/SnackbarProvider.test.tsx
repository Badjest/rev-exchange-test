import React from 'react'
import { render, fireEvent, act } from '@testing-library/react'
import configureMockStore from 'redux-mock-store'
import { Provider } from 'react-redux'

import SnackbarProvider from 'containers/SnackbarProvider'
import { removeNotification } from 'store/actions'
import { colorMap } from 'components/UI/Snackbar/Snackbar'
import { INotification } from 'store/index.d'

const INITIAL_NOTIFICATIONS = [
	{ type: 'done', content: 'Hello' },
	{ type: 'warning', content: 'Bye' },
] as INotification[]

const mockStore = configureMockStore([])
const store = mockStore({ notifications: INITIAL_NOTIFICATIONS })

describe('containers', () => {
	describe('SnackbarProvider', () => {
		it('render and store tests', () => {
			const { container } = render(
				<Provider store={store}>
					<SnackbarProvider />
				</Provider>,
			)

			const EXPECTED_ACTION = { type: removeNotification.toString(), payload: undefined }
			const expectedDivNotification = container.firstChild as HTMLDivElement

			expect(expectedDivNotification.innerHTML).toBe(INITIAL_NOTIFICATIONS[0].content)
			expect(expectedDivNotification).toHaveStyleRule('background-color', colorMap[INITIAL_NOTIFICATIONS[0].type])

			act(() => {
				fireEvent.click(expectedDivNotification)
			})

			expect(store.getActions()).toMatchObject([EXPECTED_ACTION])
		})
	})
})
