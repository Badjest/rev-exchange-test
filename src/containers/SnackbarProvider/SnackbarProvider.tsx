import React, { useCallback, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { selectNotifications } from 'store/selectors'
import { removeNotification } from 'store/actions'
import ModalPortal from 'components/ModalPortal'
import { Snackbar } from 'components/UI'

const NOTIFICATION_LIFETIME = 2000

const SnackbarProvider = () => {
	const dispatch = useDispatch()
	const notifications = useSelector(selectNotifications)

	useEffect(() => {
		if (notifications.length === 0) return

		const timer = setTimeout(() => {
			deleteLastNotification()
		}, NOTIFICATION_LIFETIME)

		return () => clearTimeout(timer)
	}, [notifications])

	const deleteLastNotification = useCallback(() => dispatch(removeNotification()), [])

	return (
		<ModalPortal>
			{notifications.length > 0 && (
				<Snackbar onClick={deleteLastNotification} snackType={notifications[0].type}>
					{notifications[0].content}
				</Snackbar>
			)}
		</ModalPortal>
	)
}

export default SnackbarProvider
