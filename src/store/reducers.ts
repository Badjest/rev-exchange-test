import { combineReducers } from 'redux'
import { saveAccounts, setRates, showNotification, removeNotification } from './actions'
import { getAccountsFromStore } from './mocks/accounts'
import { ActionType, Store, INotification } from './index.d'
import { RatesType } from 'api/index.d'

/** ACCOUNTS */
export const accountsInitialState = getAccountsFromStore()

export const accountsReducer = (state = accountsInitialState, action: ActionType<typeof accountsInitialState>) => {
	switch (action.type) {
		case saveAccounts.toString(): {
			return [...state]
		}

		default: {
			return state
		}
	}
}

/** RATES */
export const ratesReducer = (state = null, action: ActionType<RatesType>) => {
	switch (action.type) {
		case setRates.toString(): {
			return action.payload
		}

		default: {
			return state
		}
	}
}

/** NOTIFICATIONS */
export const notificationReducer = (state: INotification[] = [], action: ActionType<INotification>) => {
	switch (action.type) {
		case showNotification.toString(): {
			return [...state, action.payload]
		}

		case removeNotification.toString(): {
			return state.filter((_, i) => i !== 0)
		}

		default: {
			return state
		}
	}
}

export default combineReducers<Store>({
	accounts: accountsReducer,
	rates: ratesReducer,
	notifications: notificationReducer,
} as any)
