import { CURRENCY } from 'constants/currency'

export const LS_SAVE_KEY = '@KEY/ACCOUNTS'
export const initialAccounts = [
	{
		id: '1234',
		value: 150.44,
		currency: 'USD',
		symbol: CURRENCY.USD,
	},
	{
		id: '5678',
		value: 324,
		currency: 'EUR',
		symbol: CURRENCY.EUR,
	},
	{
		id: '9999',
		value: 100,
		currency: 'GBP',
		symbol: CURRENCY.GBP,
	},
]

export interface AccountType {
	id: string
	value: number
	currency: keyof typeof CURRENCY
	symbol: CURRENCY
}
export type AccountsType = AccountType[]

export const getAccountsFromStore = (): AccountsType => {
	const persistValue = window.localStorage && window.localStorage.getItem(LS_SAVE_KEY)

	if (persistValue) {
		return JSON.parse(String(persistValue))
	}

	return initialAccounts as AccountsType
}

export const saveAccountsToStore = (accounts: AccountsType): AccountsType => {
	if (window.localStorage) {
		window.localStorage.setItem(LS_SAVE_KEY, JSON.stringify(accounts))
	}

	return accounts
}
