import { Store } from './index.d'

/** ACCOUNTS */
export const selectAccounts = (state: Store) => state.accounts

/** RATES */
export const selectRates = (state: Store) => state.rates

/** NOTIFICATION */
export const selectNotifications = (state: Store) => state.notifications
