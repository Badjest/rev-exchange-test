import { accountsReducer, accountsInitialState, ratesReducer, notificationReducer } from 'store/reducers'
import { RATES_AFTER_FORMAT } from 'test/mockData'
import { saveAccounts, setRates, showNotification, removeNotification } from 'store/actions'
import { RatesType } from 'api'
import { INotification } from 'store/index.d'

describe('store/reducers', () => {
	describe('accountsReducer', () => {
		it('should return the initial state', () => {
			expect(accountsReducer(undefined, {} as any)).toEqual(accountsInitialState)
		})

		it('should handle saveAccounts action', () => {
			const mutationAccounts = accountsInitialState.map((el) => {
				el.value = 1222

				return el
			})

			expect(accountsReducer(accountsInitialState, saveAccounts(mutationAccounts))).toEqual(mutationAccounts)
		})
	})

	describe('ratesReducer', () => {
		it('should return null', () => {
			expect(ratesReducer(undefined, {} as any)).toBeNull()
		})

		it('should handle setRates action', () => {
			expect(ratesReducer(undefined, setRates(RATES_AFTER_FORMAT as RatesType))).toEqual(RATES_AFTER_FORMAT)
		})
	})

	describe('notificationReducer', () => {
		it('should return the initial state', () => {
			expect(notificationReducer(undefined, {} as any)).toEqual([])
		})

		const myFirstNotification: INotification = { type: 'done', content: 'Hello' }
		const mySecondNotification: INotification = { type: 'warning', content: 'Bye' }
		let notificationState: any = []

		it('should handle showNotification action', () => {
			notificationState = notificationReducer(undefined, showNotification(myFirstNotification))

			expect(notificationState).toEqual([myFirstNotification])

			notificationState = notificationReducer(
				notificationState as INotification[],
				showNotification(mySecondNotification),
			)

			expect(notificationState).toEqual([myFirstNotification, mySecondNotification])
		})

		it('should handle removeNotification action', () => {
			notificationState = notificationReducer(notificationState, removeNotification())

			expect(notificationState).toEqual([mySecondNotification])

			expect(notificationReducer(notificationState as INotification[], removeNotification())).toEqual([])
		})
	})
})
