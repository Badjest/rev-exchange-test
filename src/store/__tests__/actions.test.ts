import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import { getAccountsFromStore, LS_SAVE_KEY } from 'store/mocks/accounts'
import { exchangeAndSave, saveAccounts } from 'store/actions'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

beforeEach(() => {
	localStorage.clear()
})

afterEach(() => {
	localStorage.clear()
})

describe('store/actions', () => {
	describe('async actions', () => {
		it('update accounts when action has been done / check localStorage', async () => {
			const expectedActions = [
				{
					type: saveAccounts.toString(),
					payload: getAccountsFromStore(),
				},
			]
			const store = mockStore({ accounts: [] })

			await store.dispatch(exchangeAndSave(getAccountsFromStore()) as any)

			expect(store.getActions()).toEqual(expectedActions)
			expect(JSON.parse(localStorage.getItem(LS_SAVE_KEY) as string)).toEqual(getAccountsFromStore())
		})
	})
})
