import { SnackType } from 'components/UI/Snackbar/Snackbar.d'
import { RatesType } from 'api/index.d'

import { AccountsType } from './mocks/accounts'

export interface ActionType<T extends any> {
	type: string
	payload?: T
}

export interface INotification {
	type: SnackType
	content: React.ReactNode
}

export interface Store {
	accounts: AccountsType
	rates: RatesType
	notifications: INotification[]
}
