import thunk from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger'
import reducers from './reducers'

const { NODE_ENV } = process.env

const DEV_MODE = NODE_ENV !== 'production' && NODE_ENV !== 'test'
const middlewares = [thunk]

if (DEV_MODE) {
	middlewares.push(createLogger({ collapsed: true }) as any)
}

const store = createStore(reducers, applyMiddleware(...middlewares))

if (module.hot && DEV_MODE) {
	module.hot.accept('./reducers', () => {
		const nextRootReducer = require('./reducers').default

		store.replaceReducer(nextRootReducer)
	})
}

export default store
