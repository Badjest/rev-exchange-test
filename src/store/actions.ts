import { Dispatch } from 'redux'
import createAction from 'utils/createAction'
import { RatesType } from 'api/index'
import { AccountsType, saveAccountsToStore } from 'store/mocks/accounts'
import API from 'api/fake'
import { INotification } from './index.d'

export const saveAccounts = createAction<AccountsType>('@ACCOUNTS/SAVE')

export const setRates = createAction<RatesType>('@RATES/SET_RATES')

export const exchangeAndSave = (accounts: AccountsType) => async (dispatch: Dispatch): Promise<boolean> => {
	try {
		await API.doExchange(accounts)

		dispatch(saveAccounts(saveAccountsToStore(accounts)))

		return true
	} catch (e) {
		console.error(e)

		return false
	}
}

export const showNotification = createAction<INotification>('@NOTIFICATION/SHOW')
export const removeNotification = createAction<undefined>('@NOTIFICATION/REMOVE')
