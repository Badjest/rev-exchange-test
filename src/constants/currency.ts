export enum CURRENCY {
	USD = '$',
	EUR = '€',
	GBP = '£',
}
