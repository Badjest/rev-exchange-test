export const MAX_VALUE = 999999999999

export enum STATUSES {
	DONE = 'Money transferred successfully👌',
	WRONG = 'Something went wrong 🤷‍♂️',
	WRONG_RATES = 'Something went wrong with rates server',
}
