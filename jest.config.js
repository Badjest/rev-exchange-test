module.exports = {
	roots: ['<rootDir>/src'],
	transform: {
		'^.+\\.tsx?$': 'ts-jest',
	},
	moduleDirectories: ['node_modules', 'src'],
	automock: false,
	setupFiles: ['./src/test/setupJest.ts', 'jest-localstorage-mock'],
	setupFilesAfterEnv: ['./src/test/setupAfterEnvJest.ts'],
}
