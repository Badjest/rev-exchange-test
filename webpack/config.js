const path = require('path')
const HtmlWebPackPlugin = require('html-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')

const IS_PRODUCTION = process.env.NODE_ENV === 'production'

module.exports = {
	mode: IS_PRODUCTION ? 'production' : 'development',
	entry: './src/index.tsx',
	resolve: {
		extensions: ['.ts', '.tsx', '.js'],
		alias: { 'react-dom': '@hot-loader/react-dom' },
		modules: [path.resolve(__dirname, '../src'), 'node_modules'],
	},
	output: {
		publicPath: '/',
		path: path.join(__dirname, '../build'),
		filename: 'assets/bundle.[hash].js',
	},
	module: {
		rules: [
			{
				test: /.tsx?$/,
				loader: 'ts-loader',
				options: {
					transpileOnly: true,
				},

				exclude: [/node_modules/],
			},
			{
				test: /\.(png|jpg|jpeg|gif|svg|mp4|webm|mp3|wav|aac|ogg)$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: 'assets/[name].[sha1:hash].[ext]',
						},
					},
				],
			},
		],
	},
	optimization: {
		minimize: true,
		minimizer: [new TerserPlugin()],
	},
	plugins: [
		new HtmlWebPackPlugin({
			template: path.join(__dirname, '../src/index.html'),
		}),
	],
}
