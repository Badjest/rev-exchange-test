const merge = require('webpack-merge')
const webpackDevServerWaitpage = require('webpack-dev-server-waitpage')

const baseConfig = require('./config')

module.exports = merge(baseConfig, {
	mode: 'development',
	entry: ['react-hot-loader/patch', 'react-dev-utils/webpackHotDevClient', baseConfig.entry],
	devtool: 'eval',
	devServer: {
		host: 'localhost',
		hot: true,
		port: 3000,
		inline: true,
		open: true,
		historyApiFallback: true,
		publicPath: '/',
		before: (app, server) => {
			app.use(webpackDevServerWaitpage(server, { theme: 'dark' }))
		},
	},
})
